var gulp            = require('gulp'),
    sass            = require('gulp-sass'),
    images          = require('gulp-image'),
    pug             = require('gulp-pug'),
    sourcemaps      = require('gulp-sourcemaps'),
    babel           = require('gulp-babel'),
    autoprefixer    = require('gulp-autoprefixer'),
    concat          = require('gulp-concat'),
    notify          = require('gulp-notify'),
    uglify          = require('gulp-uglify'),
    plumber         = require('gulp-plumber'),
    rename          = require('gulp-rename'),
    browserSync     = require('browser-sync');

var path = {
    src:{
        /* ===========================  Images files    =========================== */
        img: [
            'src/images/**/*.png',
            'src/images/**/*.jpg',
            'src/images/**/*.jpeg',
            'src/images/**/*.gif',
            'src/images/**/*.svg'
        ],
        /* ===========================  JavaScript files    =========================== */
        js:{
            core:[
                'src/js/libs/jquery v2.x.js'
            ],
            script: 'src/js/script.js'
        },
        /* ===========================  SCSS files  =========================== */
        scss: {
            style: [
                'src/scss/style.scss',
                'src/scss/core/**/*.scss',
                'src/scss/components/**/*.scss',
                'src/scss/fonts/**/*.scss',
                'src/scss/modules/**/*.scss',
                'src/scss/helpers/**/*.scss'
            ],
            libs: 'src/scss/libs/**/*.scss'
        },
        /* ===========================  Pug files  =========================== */
        templates: 'src/templates/**/*.pug'
    },
    dest: {
        img:[
            '/dest/images/'
        ],
        js: {
            core: 'dest/js/',
            // JavaScript init
            script: 'dest/js/'
        },
        css:{
            style: 'dest/css/',
            libs: 'dest/css/libs/'
        },
        templates: 'dest/'
    }
};




/* ===========================  Images  =========================== */
gulp.task('dev-imageMin', function () {
    gulp.src(path.src.img)
        .pipe(images())
        .pipe(gulp.dest(path.dest.img))
});

/* ===========================  JavaScript files  =========================== */
function jsTask(taskName, src, dest, concatNameFile) {
    gulp.task(taskName, function() {
        gulp.src(src)
            .pipe(sourcemaps.init())
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(concat(concatNameFile))
            // .pipe(uglify())
            .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest(dest));
            // .pipe(browserSync.reload({stream: true}));
    });
}

jsTask('dev-jsCore', path.src.js.core, path.dest.js.core, 'core.js');
jsTask('dev-jsScript', path.src.js.script, path.dest.js.script, 'script.js');

/* ===========================  SCSS files  =========================== */
function scssTask(taskName, src, dest) {
    gulp.task(taskName, function () {
        gulp.src(src)
            .pipe(plumber({
                errorHandler: notify.onError("Error: <%= error.message %>")
            }))
            .pipe(sass())
            .pipe(autoprefixer({
                browsers: ['> 1%', 'last 15 versions'],
                cascade: false
            }))
            .pipe(plumber.stop())
            .pipe(notify("style.scss (DEV) compiled success!"))
            .pipe(gulp.dest(dest));
            // .pipe(browserSync.reload({stream: true}));
    });
}

scssTask('dev-sass', path.src.scss.style, path.dest.css.style);
scssTask('dev-sass-libs', path.src.scss.libs, path.dest.css.libs);




/* ===========================  Templates files  =========================== */
gulp.task('dev-pug', function () {
    return gulp.src(path.src.templates)
        .pipe(plumber({
            errorHandler: notify.onError("Error: <%= error.message %>")
        }))
        .pipe(pug({
            pretty: '\t'
            //debug: true
            //locals: jadedata
        }))
        .pipe(gulp.dest(path.dest.templates));

        // .pipe(browserSync.reload({stream: true}));
});

/* ===========================  Browser-Sync  =========================== */
gulp.task('browser-sync', function() {
    browserSync({
        server: {
            // baseDir: '/var/www/test/priselist/dest/' // Директория для сервера - app
            baseDir: 'dest/'
        },
        notify: false,
        open: false
    });
});


/*============= watch task =============*/
gulp.task('watch', function () {
    gulp.watch(path.src.js.core, ['dev-jsCore']);
    gulp.watch(path.src.js.script, ['dev-jsScript']);
    gulp.watch(path.src.scss.style, ['dev-sass']);
    gulp.watch(path.src.scss.libs, ['dev-sass-libs']);
    gulp.watch(path.src.templates, ['dev-pug']);
});
/*============= default task =============*/
// gulp.task('default', ['browser-sync', 'watch']);
gulp.task('default', ['watch']);
