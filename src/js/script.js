//$(window).load(function() {});
window.addEventListener( 'load', function( event ) {
    equalheight('.shop__cnt .shop__title');
});

//$(window).resize(function(){});
window.addEventListener('resize', function(event) {
    equalheight('.shop__cnt .shop__title');
    carouselHeight('.carouselFull-container');
});

document.addEventListener( 'DOMContentLoaded', function () {
    (function() {
        if (!Event.prototype.preventDefault) {
            Event.prototype.preventDefault=function() {
                this.returnValue=false;
            };
        }
        if (!Event.prototype.stopPropagation) {
            Event.prototype.stopPropagation=function() {
                this.cancelBubble=true;
            };
        }
        if (!Element.prototype.addEventListener) {
            var eventListeners=[];

            var addEventListener=function(type,listener /*, useCapture (will be ignored) */) {
                var self=this;
                var wrapper=function(e) {
                    e.target=e.srcElement;
                    e.currentTarget=self;
                    if (listener.handleEvent) {
                        listener.handleEvent(e);
                    } else {
                        listener.call(self,e);
                    }
                };
                if (type=="DOMContentLoaded") {
                    var wrapper2=function(e) {
                        if (document.readyState=="complete") {
                            wrapper(e);
                        }
                    };
                    document.attachEvent("onreadystatechange",wrapper2);
                    eventListeners.push({object:this,type:type,listener:listener,wrapper:wrapper2});

                    if (document.readyState=="complete") {
                        var e=new Event();
                        e.srcElement=window;
                        wrapper2(e);
                    }
                } else {
                    this.attachEvent("on"+type,wrapper);
                    eventListeners.push({object:this,type:type,listener:listener,wrapper:wrapper});
                }
            };
            var removeEventListener=function(type,listener /*, useCapture (will be ignored) */) {
                var counter=0;
                while (counter<eventListeners.length) {
                    var eventListener=eventListeners[counter];
                    if (eventListener.object==this && eventListener.type==type && eventListener.listener==listener) {
                        if (type=="DOMContentLoaded") {
                            this.detachEvent("onreadystatechange",eventListener.wrapper);
                        } else {
                            this.detachEvent("on"+type,eventListener.wrapper);
                        }
                        eventListeners.splice(counter, 1);
                        break;
                    }
                    ++counter;
                }
            };
            Element.prototype.addEventListener=addEventListener;
            Element.prototype.removeEventListener=removeEventListener;
            if (HTMLDocument) {
                HTMLDocument.prototype.addEventListener=addEventListener;
                HTMLDocument.prototype.removeEventListener=removeEventListener;
            }
            if (Window) {
                Window.prototype.addEventListener=addEventListener;
                Window.prototype.removeEventListener=removeEventListener;
            }
        }
    })();


    (function(ELEMENT) {
        ELEMENT.matches = ELEMENT.matches || ELEMENT.mozMatchesSelector || ELEMENT.msMatchesSelector || ELEMENT.oMatchesSelector || ELEMENT.webkitMatchesSelector;
        ELEMENT.closest = ELEMENT.closest || function closest(selector) {
                if (!this) return null;
                if (this.matches(selector)) return this;
                if (!this.parentElement) {return null}
                else return this.parentElement.closest(selector)
            };
    }(Element.prototype));




    carouselHeight('.carouselFull-container');

    bannerInit('.carouselFull-container');
    swiperInit('.stock-container');
    swiperInit('.coupon-container');
    syncCarousel();



    setId('.banner', 'banner');
    bannerListSetID();

    // Timer('May 1, 2017');
    Timer('May 1, 2017', 'finishDay__1', 'finishTime__1');
    Timer('March 1, 2017', 'finishDay__2', 'finishTime__2');

    aboutUsListPosition();
    aboutUsListOpen();

    // init d3 by id
    infoRating('news--rating-bar__1', data, 'news');
    infoRating('news--rating-bar__2', data2, 'news');

    infoRating('card--rating-bar__magazineNAME', data3, 'card');

    //init yandex map
    map__init('map');

    //init Tabs
    tabs('tabs');
    tabs('aboutus_carouselTab');

}, false );

/*
 * @function    javascript
 * @Components	Carousel height
 */
function carouselHeight(selector) {
    const containerWidth = 1180;
    let carousel = document.querySelector(selector);

    if (carousel){
        if (window.innerWidth > containerWidth){
            // console.log(carousel);
            carousel.style.height = '' + (window.innerWidth * ( 600 / 1920 ) ) + 'px';
        } else {
            carousel.style.height = ((containerWidth * 600) / 1920) + 'px';
        }
    }
}

/*
 * @function    jquery
 * @Components	Equalheight
 */
function equalheight(container){
    let currentTallest = 0,
        currentRowStart = 0,
        rowDivs = new Array(),
        topPosition = 0,
        $el;

    $(container).each(function() {

        $el = $(this);
        $($el).height('auto');
        topPosition = $el.position().top;

        if (currentRowStart != topPosition) {
            for (let currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
            rowDivs.length = 0; // empty the array
            currentRowStart = topPosition;
            currentTallest = $el.height();
            rowDivs.push($el);
        } else {
            rowDivs.push($el);
            currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
        }
        for (let currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
            rowDivs[currentDiv].height(currentTallest);
        }
    });
}

/*
 * @function    javascript
 * @Components	collectionToArray
 */
function collectionToArray(collection){
    "use strict";
    let array;
    if(!Array.isArray(collection)){
        array = Array.prototype.slice.call(collection);
        return array;
    } else {
        array = collection;
        return array;
    }
}

/*
 * @function    javascript
 * @Components	Set id by carousel
 */
function setId(selector, idName__) {
    let param = document.querySelectorAll(selector);
    let param__id = '';
    // console.log(param);
    // console.log(Array.isArray(param));
    param  = collectionToArray(param);
    // console.log(Array.isArray(param));
    // console.log(param);
    for (let i = 0; i < param.length; i++){
        param[i].setAttribute('id', idName__+'__'+( i+1 )+'');
        param__id = param[i];
    }
    return param__id;
}

/*
 * @function    javascript
 * @Components	Set id for banners who contain in carousel
 */
function setBannerIds(selector){
    let __index;
    if (~selector.indexOf("__")) {
        __index = selector.indexOf("__");
        // console.log(__index);
        if (document.getElementById(selector)){
            let __prefix = document.getElementById(selector).id.slice(__index);
            // console.log(__prefix);
            let finishDay = document.getElementById(selector).querySelector('.banner__day-to-finish');
            let finishTime = document.getElementById(selector).querySelector('.banner__time');

            if (finishDay){
                finishDay.setAttribute('id', 'finishDay'+ __prefix +'');
            }
            if (finishTime){
                finishTime.setAttribute('id', 'finishTime'+ __prefix +'');
            }

            // console.log(finishTime.innerHTML);
        } else {
            return 0;
        }
    } else {
        return 0;
    }
}
/*
 * @function    javascript
 * @Components	Bannerlist
 */
function bannerListSetID() {
    let banner = document.querySelectorAll('.banner');
    collectionToArray(banner);
    for(let i = 0; i < banner.length; i++){
        // console.log(banner[i].getAttribute('id'));
        setBannerIds(banner[i].getAttribute('id'));
    }
}

/*
 * @function    jquery
 * @Components	Swiper
 */

function syncCarousel() {

    var swiper_horizontal = new Swiper(".syncCarousel__horizontal", {
        direction: 'horizontal',
        slidesPerView: '1',
        spaceBetween: 10,
        grabCursor: true,
    });

    var swiper_vertical = new Swiper(".syncCarousel__vertical", {
        direction: 'vertical',
        slidesPerView: 3,
        spaceBetween: 10,
        grabCursor: true,
        centeredSlides: true,

        nextButton: '.syncCarousel__button--next',
        prevButton: '.syncCarousel__button--prev'
    });

    swiper_horizontal.params.control = swiper_vertical;
    swiper_vertical.params.control = swiper_horizontal;
}




function swiperInit(selector){
    let swiper = new Swiper(selector, {
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        paginationClickable: true,
        slidesPerView: 4,
        spaceBetween: 20,
        // loop: true
    });
}
function bannerInit(selector) {
    const containerWidth = 1180;
    const paddingOffset = 200;

    let pagination = document.querySelector('.carouselFull__pagination');

    let swiper = new Swiper(selector, {
        pagination: '.carouselFull__pagination',
        paginationClickable: true,
        effect: 'fade',
        fade: {
            crossFade: false
        },
        centeredSlides: true,
        // autoplay: 2500,
        autoplayDisableOnInteraction: false
    });
    function paginationPosition(pagination) {
        if(pagination) {
            if (window.innerWidth < 1600){
                pagination.style.left = ( (window.innerWidth - containerWidth) / 2) + paddingOffset + 'px';
            } else {
                pagination.style.left = (window.innerWidth - containerWidth) / 2 + 'px';
            }
        }
    }

    paginationPosition(pagination);

    window.addEventListener('resize', function(event) {
        event.preventDefault();
        paginationPosition(pagination);
    });
}





function Timer (date, __dayID, __TimeID) {
    if ( document.getElementById(__dayID) && document.getElementById(__TimeID) ){
        let seconds = (new Date(date)).getTime() - (new Date()).getTime();
        seconds = parseInt(seconds / 1000);

        let days = parseInt(seconds / 86400);
        function getDays() {
            return days;
        }
        seconds -= getDays(days) * 86400;

        let hours = parseInt(seconds / 3600);
        seconds -= hours * 3600;

        let minutes = parseInt(seconds / 60);
        seconds -= minutes * 60;

        let arrFinish = [days, hours, minutes, seconds];
        // document.getElementById('finishDay__1').innerHTML = arrFinish.shift();
        // document.getElementById('finishTime__1').innerHTML = arrFinish.slice(-3).join(':');
        document.getElementById(__dayID).innerText = arrFinish.shift();
        document.getElementById(__TimeID).innerText = arrFinish.slice(-3).join(':');

        // console.log(days + 'd:' +hours + 'h:' + minutes + 'm:' + seconds);
        if ( (new Date(date)).getTime() >= (new Date()).getTime() ){
            setTimeout('Timer(\'' + date + '\', \'' + __dayID + '\', \'' + __TimeID + '\');', 1000);
        }
    }
}

function aboutUsListPosition() {
    let aboutusList = document.querySelector('.about-us__list');

    if(aboutusList) {
        // console.log(aboutusList.style.top = (aboutusList.clientHeight));
        // console.log(aboutusList.style.top = (aboutusList.clientWidth));
        aboutusList.style.top = -20 - (aboutusList.clientHeight)+'px';
    }
}

function aboutUsListOpen() {
    let aboutusList = document.querySelector('.about-us__list');
    let aboutusBtn = document.querySelector('.about-us__btn');

    if (aboutusBtn){
        aboutusBtn.addEventListener('click', function (event){
            // console.log(event.target);
            event.preventDefault();

            if(aboutusList) {
                aboutusList.classList.toggle('about-us__list--opened');
            }
        });
    }

}



const data = [
    {"rate":5,	"count":35},
    {"rate":4,	"count":26},
    {"rate":3,	"count":8},
    {"rate":2,	"count":1},
    {"rate":1,	"count":6},
];

const data2 = [
    {"rate":5,	"count":25},
    {"rate":4,	"count":26},
    {"rate":3,	"count":1},
    {"rate":2,	"count":1},
    {"rate":1,	"count":1},
];

const data3 = [
    {"rate":5,	"count":125},
    {"rate":4,	"count":22},
    {"rate":3,	"count":18},
    {"rate":2,	"count":2},
    {"rate":1,	"count":3},
];

function infoRating(id, data, wrapper){
    if ( document.getElementById(id) ){
        // const rating = document.getElementById(id).parentElement.parentElement
        const rating = document.getElementById(id).closest('.rating');
        // const rating__avr = document.getElementById(id).parentElement.parentElement.children[0].children[0];
        const rating__avr = rating.children[0].children[0];
        const rating__count =  rating.children[0].children[1].children[0];
        const rating__bars = document.getElementById(id).parentElement;
        const colors = ['#4f9c37', '#81d77f', '#efd41e', '#dbb15f', '#dc7922'];
        const w = rating__bars.clientWidth;
        const h = rating__bars.clientHeight;

        let margin  = {top: 15, right: 15, bottom: 15, left: 0};
        let width   =  w - (margin.left + margin.right);
        let height  = h - (margin.top + margin.bottom);

        const barAvailHeight = height / data.length,
            bar__spasing = {top: 1, bottom: 2},
            barHeight = barAvailHeight - (bar__spasing.top + bar__spasing.bottom);
        const barHeight__offset = 5;
        const barText__offset = 30;

        // console.log(rating__bars.children[0].id);
        // console.log(w);
        // console.log(h);
        let widthScale = d3.scale.linear()
            .domain([
                d3.min(data, function(d, i) {
                    // console.log(height - margin.top - margin.bottom);
                    // return d.count;
                    return 0;
                }),
                d3.max(data, function(d, i) {
                    return d.count;
                })
            ])
            .range([0, width - barText__offset]);


        let svg_wraper = d3.select(rating__bars)
            .style("padding-top", margin.top+'px')
            .style("padding-left", margin.left+'px')
            .style("padding-right", margin.right+'px')
            .style("padding-bottom", margin.bottom+'px');

        // let svg = d3.select('#' + rating__bars.children[0].id +'')
        let svg = d3.select('#' + id)
            .attr("width", width)
            .attr("height", height);


        let bars = svg
            .append('g')
            .attr('class', 'rect')
            .selectAll('rect')
            .data(data)
            .enter()
            .append('g')
            .attr('class', 'rect__item');

        let bars__text = bars
                .append('text')
                .attr('class', 'rect__text')
                .attr('x', width)
                .attr('y', function(d,i) {
                    return (i+1)*8;
                })
                .attr("text-anchor", "end")
                .style('font-size', '9px')
                // .style('fill' ,'#bbbaba')
                .style('fill' ,'#000000')
                .text(function(d,i) { return d.count; })

            ;

        let bars__item = bars
                .append('rect')
                .attr('class', function(d, i){
                    return 'rect__item--' + d.rate;
                })
                .attr('data-count', function(d, i){
                    return d.count;
                })
                .attr('data-rate', function(d, i){
                    return d.rate;
                })
                .attr('fill', function(d, i) {
                    // console.log(colors)
                    return colors[i];
                })
                .attr('width', 0).transition().duration(1500)
                .attr('width', function(d,i) {
                    return widthScale(d.count);
                })
                .attr('height', barHeight)

                .attr('y', function(d,i) {
                    return i*barAvailHeight;
                })
            ;


        function summRewiews(data) {
            let dataCount__summ = 0;

            for (let i = 0; i < data.length; i++){
                dataCount__summ += data[i].count;
            }
            return dataCount__summ;
        }
        function avrRewiews(data){
            let avr__val = 0;
            let countRewiews = summRewiews(data);

            for (let i = 0; i < data.length; i++){
                avr__val += (data[i].count * data[i].rate) / countRewiews;
            }
            return avr__val.toFixed(1);
        }

        const information__likes = document.getElementById(id).closest('.'+ wrapper).querySelector(' .information .information__likes');
        const information__likesVal = document.getElementById(id).closest('.'+ wrapper).querySelector(' .information .information__likes-val');

        function setValHTML(){
            rating__count.textContent = summRewiews(data);
            rating__avr.textContent = avrRewiews(data);

            information__likesVal.innerText = avrRewiews(data);
        }
        setValHTML();


        function openInfoLike(event) {
            event.preventDefault();
            let __self = this;
            let __selfRating = __self.closest('.'+wrapper).querySelector('.rating');

            if ( __selfRating.classList.contains('rating--opened') ){
                __selfRating.classList.remove('rating--opened');
            } else {
                __selfRating.classList.add('rating--opened');
            }
        }


        information__likes.addEventListener('click', openInfoLike, false);
    }
}

/*
 * @function    Yandex Api
 * @Components	Yandex map function init
 */
function map__init(id){
    if ( document.getElementById(id) ){
        ymaps.ready(init);
        let map;

        function init(){
            map = new ymaps.Map (id, {
                center: [55.76, 37.64],
                zoom: 7
            });
        }
    }
}

/*
 * @function    javascript
 * @Components	Tabs init
 */
function tabs(id) {

    let tab = document.getElementById(id);

    if (tab){
        let tab__navLinks = document.querySelectorAll('#'+id+' > .tabs__nav .tabs__nav-link');
        let tab__cntContainers = document.querySelectorAll('#'+id+' > .tabs__cnt');
        var activeIndex = 0;

        for (let i = 0; i < tab__navLinks.length; i++) {
            let link = tab__navLinks[i];
            link.addEventListener('click', function(e) {
                e.preventDefault();
                if (i !== activeIndex && i >= 0 && i <= tab__navLinks.length) {
                    tab__navLinks[activeIndex].classList.remove('tabs__nav-link--active');
                    tab__navLinks[i].classList.add('tabs__nav-link--active');
                    tab__cntContainers[activeIndex].classList.remove('tabs__cnt--active');
                    tab__cntContainers[i].classList.add('tabs__cnt--active');
                    activeIndex = i;
                }
            });
        }
    }
}